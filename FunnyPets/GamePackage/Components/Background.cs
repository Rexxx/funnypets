﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FunnyPets.GamePackage.Components
{
    class Background : IComponent
    {
        private readonly Texture2D texture;
        private readonly Rectangle rectangle;

        public Background(Texture2D texture, Rectangle rectangle)
        {
            this.texture = texture ?? throw new ArgumentNullException(nameof(texture));
            this.rectangle = rectangle;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rectangle, Color.White);
        }

        public void Update(GameTime gameTime)
        {
            //nothing to do here
        }
    }
}
