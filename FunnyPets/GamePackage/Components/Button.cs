﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FunnyPets.GamePackage.Components
{
    class Button : IComponent
    {
        public event EventHandler Click;
        private MouseState previousMouse;
        private readonly Texture2D texture;
        private readonly SpriteFont font;
        public string Text { get; }
        private bool IsHovering { get; set; }
        public bool Enabled { get; set; }
        private readonly Rectangle rectangle;
        private readonly Vector2 textPosition;

        public Button(Texture2D texture, SpriteFont font, string text, Vector2 position)
        {
            this.texture = texture ?? throw new ArgumentNullException(nameof(texture));
            this.font = font ?? throw new ArgumentNullException(nameof(font));
            this.Text = text;

            rectangle = new Rectangle((int)(position.X - texture.Width / 2), (int)(position.Y - texture.Height / 2), texture.Width, texture.Height);
            var textMeasurements = font.MeasureString(text);
            textPosition = new Vector2(position.X - textMeasurements.X / 2, position.Y - textMeasurements.Y / 2);
            Enabled = true;
        }

        public Button(Texture2D texture, Vector2 position, int width, int height)
        {
            this.texture = texture;
            rectangle = new Rectangle((int)(position.X - width / 2), (int)(position.Y - height / 2), width, height);
            Enabled = true;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Enabled)
            {
                if (IsHovering)
                {
                    spriteBatch.Draw(texture, rectangle, Color.LightGray);
                }
                else
                {
                    spriteBatch.Draw(texture, rectangle, Color.White);
                }
            }
            else
            {
                spriteBatch.Draw(texture, rectangle, Color.Gray);
            }

            if (!string.IsNullOrEmpty(Text))
            {
                spriteBatch.DrawString(font, Text, textPosition, Color.Black);
            }
        }

        public void Update(GameTime gameTime)
        {
            if (!Enabled)
            {
                return;
            }

            var currentMouse = Mouse.GetState();
            IsHovering = false;

            if (rectangle.Contains(currentMouse.X, currentMouse.Y))
            {
                IsHovering = true;

                if (currentMouse.LeftButton == ButtonState.Released && previousMouse.LeftButton == ButtonState.Pressed)
                {
                    Click?.Invoke(this, new EventArgs());
                }
            }

            previousMouse = currentMouse;
        }
    }
}
