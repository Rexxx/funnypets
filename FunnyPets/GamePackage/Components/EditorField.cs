﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FunnyPets.GamePackage.Components
{
    class EditorField : MapField, IComponent
    {
        private MouseState previousMouse;
        private Map newMap;
        public Map NewMap
        {
            get
            {
                return newMap;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                var entities = Map.CopyEntities(value.Entities);
                newMap = new Map(entities, value.Name);

                entitySize = ComputeEntitySize(entities);
                mapRectangle = ComputeMapRectangle(entities);
            }
        }
        public Entity ActiveEntity { private get; set; }
        public bool IsMapCorrect => Map.IsCorrectMap(NewMap.Entities);

        public EditorField(Rectangle rectangle, Texture2D checkMark, Texture2D crossMark)
        {
            this.fieldRectangle = rectangle;
            this.checkMark = checkMark ?? throw new ArgumentNullException(nameof(checkMark));
            this.crossMark = crossMark ?? throw new ArgumentNullException(nameof(crossMark));

            ActiveEntity = Entity.Blank;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var entities = NewMap.Entities;
            var height = entities.GetLength(0);
            var width = entities.GetLength(1);

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    var entity = entities[i, j];

                    if (entity == Entity.Blank)
                    {
                        continue;
                    }

                    var currentRect = ComputeEntityRectangle(i, j);
                    spriteBatch.Draw(GetTextureByEntity(entity), currentRect, Color.White);
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            var currentMouse = Mouse.GetState();

            var entities = NewMap.Entities;
            var height = entities.GetLength(0);
            var width = entities.GetLength(1);

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    var entity = entities[i, j];
                    var currentRect = ComputeEntityRectangle(i, j);

                    if (currentRect.Contains(currentMouse.X, currentMouse.Y))
                    {
                        if (currentMouse.LeftButton == ButtonState.Released && previousMouse.LeftButton == ButtonState.Pressed)
                        {
                            NewMap.EditEntity(i, j, ActiveEntity);
                        }
                    }
                }
            }

            previousMouse = currentMouse;
        }
    }
}
