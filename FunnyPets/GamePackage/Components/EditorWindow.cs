﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace FunnyPets.GamePackage.Components
{
    class EditorWindow : IComponent
    {
        private readonly Button buttonBack;
        private readonly Button buttonSave;
        private readonly Button buttonPlayer, buttonBox, buttonBoxOnGoal, buttonWall, buttonGoal, buttonBlank;
        private readonly EditorField editorField;
        public Map NewMap => editorField.NewMap;

        public EditorWindow(Button buttonBack, Button buttonSave, Button btnPlayer, Button btnBox, Button btnBoxOnGoal, Button btnWall, 
            Button btnGoal, Button btnBlank, EditorField editorField)
        {
            this.buttonBack = buttonBack ?? throw new ArgumentNullException(nameof(buttonBack));
            this.buttonSave = buttonSave ?? throw new ArgumentNullException(nameof(buttonSave));
            this.buttonPlayer = btnPlayer ?? throw new ArgumentNullException(nameof(btnPlayer));
            buttonPlayer.Click += ButtonPlayer_OnClick;
            this.buttonBox = btnBox ?? throw new ArgumentNullException(nameof(btnBox));
            buttonBox.Click += ButtonBox_OnClick;
            this.buttonBoxOnGoal = btnBoxOnGoal ?? throw new ArgumentNullException(nameof(btnBoxOnGoal));
            buttonBoxOnGoal.Click += ButtonBoxOnGoal_OnClick;
            this.buttonWall = btnWall ?? throw new ArgumentNullException(nameof(btnWall));
            buttonWall.Click += ButtonWall_OnClick;
            this.buttonGoal = btnGoal ?? throw new ArgumentNullException(nameof(btnGoal));
            buttonGoal.Click += ButtonGoal_OnClick;
            this.buttonBlank = btnBlank ?? throw new ArgumentNullException(nameof(btnBlank));
            buttonBlank.Click += ButtonBlank_OnClick;
            this.editorField = editorField ?? throw new ArgumentNullException(nameof(editorField));
        }

        public void SetupMap(Map map)
        {
            editorField.NewMap = map;
        }

        public void LoadUserSkins(Texture2D player, Texture2D box, Texture2D wall)
        {
            editorField.LoadUserSkins(player, box, wall);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            buttonBack.Draw(gameTime, spriteBatch);
            buttonSave.Draw(gameTime, spriteBatch);
            buttonPlayer.Draw(gameTime, spriteBatch);
            buttonBox.Draw(gameTime, spriteBatch);
            buttonBoxOnGoal.Draw(gameTime, spriteBatch);
            buttonWall.Draw(gameTime, spriteBatch);
            buttonGoal.Draw(gameTime, spriteBatch);
            buttonBlank.Draw(gameTime, spriteBatch);
            editorField.Draw(gameTime, spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            editorField.Update(gameTime);
            buttonSave.Enabled = editorField.IsMapCorrect;

            buttonBack.Update(gameTime);
            buttonSave.Update(gameTime);
            buttonPlayer.Update(gameTime);
            buttonBox.Update(gameTime);
            buttonBoxOnGoal.Update(gameTime);
            buttonWall.Update(gameTime);
            buttonGoal.Update(gameTime);
            buttonBlank.Update(gameTime);
        }

        private void ButtonPlayer_OnClick(object sender, EventArgs e)
        {
            editorField.ActiveEntity = Entity.Player;
        }

        private void ButtonBox_OnClick(object sender, EventArgs e)
        {
            editorField.ActiveEntity = Entity.Box;
        }

        private void ButtonBoxOnGoal_OnClick(object sender, EventArgs e)
        {
            editorField.ActiveEntity = Entity.BoxOnGoal;
        }

        private void ButtonWall_OnClick(object sender, EventArgs e)
        {
            editorField.ActiveEntity = Entity.Wall;
        }

        private void ButtonGoal_OnClick(object sender, EventArgs e)
        {
            editorField.ActiveEntity = Entity.Goal;
        }

        private void ButtonBlank_OnClick(object sender, EventArgs e)
        {
            editorField.ActiveEntity = Entity.Blank;
        }
    }
}
