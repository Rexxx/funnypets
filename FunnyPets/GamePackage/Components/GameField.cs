﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FunnyPets.GamePackage.Components
{
    class GameField : MapField, IComponent
    {
        private Level level;
        public Level Level {
            get { return level; }
            set
            {
                level = value ?? throw new ArgumentNullException(nameof(value));
                var entities = level.CurrentMap;

                entitySize = ComputeEntitySize(Level.CurrentMap);
                mapRectangle = ComputeMapRectangle(Level.CurrentMap);
            }
        }
        private double prevActionTimeInSec;
        private List<Keys> lastPressed;
        public bool IsGameFinished => Level.IsLevelCompleted();
        public int StepsCount => Level.StepsCount;
        public double TimeInSec => Level.TimeInSec;
        private const double pressedTimeDelayInSec = 0.2;

        public GameField(Rectangle rectangle,  Texture2D checkMark, Texture2D crossMark)
        {
            
            this.fieldRectangle = rectangle;
            this.checkMark = checkMark ?? throw new ArgumentNullException(nameof(checkMark));
            this.crossMark = crossMark ?? throw new ArgumentNullException(nameof(crossMark));

            lastPressed = new List<Keys>();
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var entities = Level.CurrentMap;
            var height = entities.GetLength(0);
            var width = entities.GetLength(1);

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    var entity = entities[i, j];

                    if (entity == Entity.Blank)
                    {
                        continue;
                    }

                    var currentRect = ComputeEntityRectangle(i, j);
                    spriteBatch.Draw(GetTextureByEntity(entity), currentRect, Color.White);

                    if (entity == Entity.BoxOnGoal)
                    {
                        spriteBatch.Draw(checkMark, currentRect, Color.White);
                    }
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            if (IsGameFinished)
            {
                return;
            }

            Level.TimeInSec += gameTime.ElapsedGameTime.TotalSeconds;
            var currentPressed = Keyboard.GetState().GetPressedKeys();

            if (currentPressed.Length == 0)
            {
                lastPressed.Clear();
                return;
            }

            if (gameTime.TotalGameTime.TotalSeconds - prevActionTimeInSec > pressedTimeDelayInSec)
            {
                lastPressed.Clear();
            }

            foreach (var key in currentPressed)
            {
                if (!lastPressed.Contains(key))
                {
                    if (key.Equals(Keys.Up))
                    {
                        Level.TryToMovePlayer(Direction.Up);
                    }
                    else if (key.Equals(Keys.Down))
                    {
                        Level.TryToMovePlayer(Direction.Down);
                    }
                    else if (key.Equals(Keys.Left))
                    {
                        Level.TryToMovePlayer(Direction.Left);

                    }
                    else if (key.Equals(Keys.Right))
                    {
                        Level.TryToMovePlayer(Direction.Right);
                    }

                    prevActionTimeInSec = gameTime.TotalGameTime.TotalSeconds;
                }
            }

            lastPressed = new List<Keys>(currentPressed);
        }
    }
}
