﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FunnyPets.GamePackage.Components
{
    class GameWindow : IComponent
    {
        private readonly Button buttonBack;
        private readonly Button buttonContinue;
        private readonly Label labelTime;
        private readonly Label labelSteps;
        private readonly GameField gameField;

        public GameWindow(Button buttonBack, Button buttonContinue, Label labelTime, Label labelSteps, GameField gameField)
        {
            this.buttonBack = buttonBack ?? throw new ArgumentNullException(nameof(buttonBack));
            this.buttonContinue = buttonContinue ?? throw new ArgumentNullException(nameof(buttonContinue));
            this.labelTime = labelTime ?? throw new ArgumentNullException(nameof(labelTime));
            this.labelSteps = labelSteps ?? throw new ArgumentNullException(nameof(labelSteps));
            this.gameField = gameField ?? throw new ArgumentNullException(nameof(gameField));
        }

        public void SetupMap(Map map)
        {
            gameField.Level = new Level(map);
        }

        public void LoadUserSkins(Texture2D player, Texture2D box, Texture2D wall)
        {
            gameField.LoadUserSkins(player, box, wall);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            buttonBack.Draw(gameTime, spriteBatch);
            buttonContinue.Draw(gameTime, spriteBatch);
            labelTime.Draw(gameTime, spriteBatch);
            labelSteps.Draw(gameTime, spriteBatch);
            gameField.Draw(gameTime, spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            gameField.Update(gameTime);
            labelTime.Text = $"Время: {(int)gameField.TimeInSec}";
            labelSteps.Text = $"Шаги: {gameField.StepsCount}";
            buttonContinue.Enabled = gameField.IsGameFinished;

            buttonBack.Update(gameTime);
            buttonContinue.Update(gameTime);
            labelTime.Update(gameTime);
            labelSteps.Update(gameTime);
        }
    }
}
