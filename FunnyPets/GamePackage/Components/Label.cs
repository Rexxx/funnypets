﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FunnyPets.GamePackage.Components
{
    class Label : IComponent
    {
        private readonly SpriteFont font;
        public string Text { get; set; }
        private readonly Vector2 position;
        private readonly Vector2[] stroke;

        public Label(SpriteFont font, string text, Vector2 position, bool isDrawFromCenter)
        {
            this.font = font ?? throw new ArgumentNullException(nameof(font));
            this.Text = text ?? throw new ArgumentNullException(nameof(text));
            var strokeWidth = 3;

            if (isDrawFromCenter)
            {
                var textMeasurements = font.MeasureString(text);
                this.position = new Vector2(position.X - textMeasurements.X / 2, position.Y - textMeasurements.Y / 2);
            }
            else
            {
                this.position = position;
            }

            stroke = new[]
            {
                new Vector2(this.position.X - strokeWidth, this.position.Y - strokeWidth),
                new Vector2(this.position.X - strokeWidth, this.position.Y),
                new Vector2(this.position.X - strokeWidth, this.position.Y + strokeWidth),
                new Vector2(this.position.X, this.position.Y - strokeWidth),
                new Vector2(this.position.X, this.position.Y + strokeWidth),
                new Vector2(this.position.X + strokeWidth, this.position.Y - strokeWidth),
                new Vector2(this.position.X + strokeWidth, this.position.Y),
                new Vector2(this.position.X + strokeWidth, this.position.Y + strokeWidth)
            };
        }

        public Label(SpriteFont font, string text, Vector2 position)
            : this(font, text, position, false)
        {
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var scaleCoeff = 1;

            foreach (var strokePosition in stroke)
            {
                spriteBatch.DrawString(font, Text, strokePosition, Color.Black, 0, Vector2.Zero, scaleCoeff, SpriteEffects.None, 0);
            }

            spriteBatch.DrawString(font, Text, position, Color.White, 0, Vector2.Zero, scaleCoeff, SpriteEffects.None, 0);
        }

        public void Update(GameTime gameTime)
        {
            //nothing to do here
        }
    }
}
