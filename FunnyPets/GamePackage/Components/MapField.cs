﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FunnyPets.GamePackage.Components
{
    class MapField
    {
        protected Texture2D player, box, wall;
        protected Texture2D checkMark, crossMark;
        protected Rectangle fieldRectangle, mapRectangle;
        protected int entitySize;

        public void LoadUserSkins(Texture2D player, Texture2D box, Texture2D wall)
        {
            this.player = player ?? throw new ArgumentNullException(nameof(player));
            this.box = box ?? throw new ArgumentNullException(nameof(box));
            this.wall = wall ?? throw new ArgumentNullException(nameof(wall));
        }

        protected int ComputeEntitySize(Entity[,] entities)
        {
            var rowsCount = entities.GetLength(0);
            var colsCount = entities.GetLength(1);

            return Math.Min(fieldRectangle.Width / colsCount, fieldRectangle.Height / rowsCount);
        }

        protected Rectangle ComputeMapRectangle(Entity[,] entities)
        {
            var rowsCount = entities.GetLength(0);
            var colsCount = entities.GetLength(1);
            var marginX = (fieldRectangle.Width - entitySize * colsCount) / 2;
            var marginY = (fieldRectangle.Height - entitySize * rowsCount) / 2;

            return new Rectangle(fieldRectangle.X + marginX, fieldRectangle.Y + marginY,
                fieldRectangle.Width - marginX, fieldRectangle.Height - marginY);
        }

        protected Rectangle ComputeEntityRectangle(int row, int col)
        {
            return new Rectangle(mapRectangle.X + col * entitySize, mapRectangle.Y + row * entitySize, entitySize, entitySize);
        }

        protected Texture2D GetTextureByEntity(Entity entity)
        {
            switch (entity)
            {
                case Entity.Wall:
                    return wall;
                case Entity.Box:
                    return box;
                case Entity.BoxOnGoal:
                    return box;
                case Entity.Goal:
                    return crossMark;
                case Entity.Player:
                    return player;
                case Entity.PlayerOnGoal:
                    return player;
                default:
                    throw new Exception("Incorrect entity without texture");
            }
        }
    }
}
