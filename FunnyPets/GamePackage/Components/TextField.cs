﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FunnyPets.GamePackage.Components
{
    class TextField : IComponent
    {
        private readonly Texture2D texture;
        private readonly SpriteFont font;
        private readonly Rectangle rectangle;
        public string Text { get; private set; }
        private List<Keys> lastPressed;
        private double lastPressedTimeInSec;
        private const int TEXT_MARGIN = 15;
        private const int MAX_TEXT_LENGTH = 16;
        private readonly Vector2 textPosition;
        private const double pressedTimeDelayInSec = 0.2;

        public TextField(Texture2D texture, SpriteFont font, Vector2 position)
        {
            this.texture = texture ?? throw new ArgumentNullException(nameof(texture));
            this.font = font ?? throw new ArgumentNullException(nameof(font));
            Text = string.Empty;
            rectangle = new Rectangle((int)(position.X - texture.Width / 2), (int)(position.Y - texture.Height / 2), texture.Width, texture.Height);
            textPosition = new Vector2(rectangle.X + TEXT_MARGIN, rectangle.Y + TEXT_MARGIN);
            lastPressed = new List<Keys>();
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rectangle, Color.White);
            spriteBatch.DrawString(font, Text, textPosition, Color.Black);
        }

        public void Update(GameTime gameTime)
        {
            var currentPressed = Keyboard.GetState().GetPressedKeys();

            if (currentPressed.Length == 0)
            {
                lastPressed.Clear();
                return;
            }

            if (gameTime.TotalGameTime.TotalSeconds - lastPressedTimeInSec > pressedTimeDelayInSec)
            {
                lastPressed.Clear();
            }

            foreach (var key in currentPressed)
            {
                if (!lastPressed.Contains(key))
                {
                    if (key.ToString().Length == 1)
                    {
                        if (Text.Length < MAX_TEXT_LENGTH)
                        {
                            Text += key;
                        }
                    }
                    else if (key.Equals(Keys.Back))
                    {
                        if (Text != string.Empty)
                        {
                            Text = Text.Remove(Text.Length - 1, 1);
                        }
                    }

                    lastPressedTimeInSec = gameTime.TotalGameTime.TotalSeconds;
                }
            }

            lastPressed = new List<Keys>(currentPressed);
        }
    }
}
