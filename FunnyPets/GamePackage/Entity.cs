﻿namespace FunnyPets.GamePackage
{
    enum Entity
    {
        Player,
        Box,
        Wall,
        Goal,
        Blank,
        BoxOnGoal,
        PlayerOnGoal
    }
}
