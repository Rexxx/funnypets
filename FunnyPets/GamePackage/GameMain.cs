﻿using System;
using System.Collections.Generic;
using System.IO;
using FunnyPets.GamePackage.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FunnyPets.GamePackage
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameMain : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private GameState state;
        private readonly Dictionary<GameState, List<IComponent>> stateComponents;
        private User user;
        private readonly GameMaps gameMaps;
        private readonly List<Texture2D> playerSkins, boxSkins, wallSkins;
        private EditorWindow editorWindow;
        private Components.GameWindow gameWindow;

        public GameMain()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1024,
                PreferredBackBufferHeight = 720
            };
            Content.RootDirectory = "Content";
            stateComponents = new Dictionary<GameState, List<IComponent>>(10);
            gameMaps = new GameMaps($"{Content.RootDirectory}/level");
            playerSkins = new List<Texture2D>();
            boxSkins = new List<Texture2D>();
            wallSkins = new List<Texture2D>();
            state = GameState.NAME_INPUT;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            var headerFont = Content.Load<SpriteFont>("font/berlin");
            var font = Content.Load<SpriteFont>("font/courier");
            var textFieldImage = Content.Load<Texture2D>("component/textfield");
            var buttonTexture = Content.Load<Texture2D>("component/button");
            var backgroundMainMenuTexture = Content.Load<Texture2D>("background/bg-menu");
            var backgroundPlayMenuTexture = Content.Load<Texture2D>("background/bg-play");
            var backgroundEditorMenuTexture = Content.Load<Texture2D>("background/bg-editor");
            var checkMark = Content.Load<Texture2D>("mark/check");
            var crossMark = Content.Load<Texture2D>("mark/cross");
            var trashMark = Content.Load<Texture2D>("mark/trash");

            LoadSkins();
            InitNameInputMenu(headerFont, font, textFieldImage, buttonTexture, backgroundMainMenuTexture);
            InitMainMenu(headerFont, font, buttonTexture, backgroundMainMenuTexture);
            InitPlayMenu(headerFont, font, buttonTexture, backgroundPlayMenuTexture);
            InitSelectLevelPlayMenu(headerFont, font, buttonTexture, backgroundPlayMenuTexture);
            InitGameWindow(buttonTexture, font, checkMark, crossMark);
            InitEditorMenu(headerFont, font, buttonTexture, backgroundEditorMenuTexture);
            InitSelectLevelEditorMenu(headerFont, font, buttonTexture, backgroundEditorMenuTexture);
            InitEditorWindow(buttonTexture, font, playerSkins[0], boxSkins[0], checkMark, wallSkins[0], crossMark, trashMark);
            InitSkinsMenu(headerFont, font, buttonTexture);
        }

        private void LoadSkins()
        {
            playerSkins.Add(Content.Load<Texture2D>("player/cat"));
            playerSkins.Add(Content.Load<Texture2D>("player/bull"));
            playerSkins.Add(Content.Load<Texture2D>("player/dog"));
            playerSkins.Add(Content.Load<Texture2D>("player/duck"));
            playerSkins.Add(Content.Load<Texture2D>("player/elephant"));
            playerSkins.Add(Content.Load<Texture2D>("player/fish"));
            playerSkins.Add(Content.Load<Texture2D>("player/lion"));
            playerSkins.Add(Content.Load<Texture2D>("player/monkey"));
            playerSkins.Add(Content.Load<Texture2D>("player/tiger"));
            playerSkins.Add(Content.Load<Texture2D>("player/wolf"));

            boxSkins.Add(Content.Load<Texture2D>("box/tennis"));
            boxSkins.Add(Content.Load<Texture2D>("box/american"));
            boxSkins.Add(Content.Load<Texture2D>("box/basket"));
            boxSkins.Add(Content.Load<Texture2D>("box/bowling"));
            boxSkins.Add(Content.Load<Texture2D>("box/box"));
            boxSkins.Add(Content.Load<Texture2D>("box/pokemon"));
            boxSkins.Add(Content.Load<Texture2D>("box/pool"));
            boxSkins.Add(Content.Load<Texture2D>("box/soccer"));
            boxSkins.Add(Content.Load<Texture2D>("box/volley"));

            wallSkins.Add(Content.Load<Texture2D>("wall/brick"));
        }

        private void InitNameInputMenu(SpriteFont headerFont, SpriteFont font, Texture2D textFieldImage, Texture2D buttonImage, Texture2D backgroundTexture)
        {
            var screenBorderMargin = 40;
            var topMargin = 300;
            var componentMargin = 10;

            var header = new Label(headerFont, "Authorization", new Vector2(screenBorderMargin, screenBorderMargin));
            var background = new Background(backgroundTexture, 
                new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            var componentPosition = new Vector2(graphics.PreferredBackBufferWidth / 2, topMargin);
            var positionOffsetY = buttonImage.Height + componentMargin;
            var labelMsg = new Label(font, "Представьтесь", componentPosition, true);
            componentPosition.Y += positionOffsetY;

            var textFieldName = new TextField(textFieldImage, font, componentPosition);
            componentPosition.Y += positionOffsetY;

            var buttonNameOk = new Button(buttonImage, font, "Ok", componentPosition);
            buttonNameOk.Click += ButtonNameOk_OnClick;

            stateComponents[GameState.NAME_INPUT] = new List<IComponent>()
            {
                background,
                header,
                labelMsg,
                textFieldName,
                buttonNameOk
            };
        }

        private void ButtonNameOk_OnClick(object sender, EventArgs e)
        {
            string name = null;

            foreach (var component in stateComponents[GameState.NAME_INPUT])
            {
                if (component is TextField)
                {
                    name = ((TextField)component).Text;
                }
            }

            if (name != null && name != string.Empty)
            {
                state = GameState.MAIN_MENU;
                user = new User(name);
                UpdateSkinsMenu();
            }
        }

        private void InitMainMenu(SpriteFont headerFont, SpriteFont buttonFont, Texture2D buttonImage, Texture2D backgroundTexture)
        {
            var screenBorderMargin = 40;
            var menuTopMargin = 300;
            var buttonMargin = 10;

            var header = new Label(headerFont, "Funny pets", new Vector2(screenBorderMargin, screenBorderMargin));
            var background = new Background(backgroundTexture,
                new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            var buttonPosition = new Vector2(graphics.PreferredBackBufferWidth / 2, menuTopMargin);
            var buttonPositionOffsetY = buttonImage.Height + buttonMargin;

            var buttonCampaign = new Button(buttonImage, buttonFont, "Кампания", buttonPosition);
            buttonCampaign.Click += ButtonCampaign_OnClick;
            buttonPosition.Y += buttonPositionOffsetY;

            var buttonEditor = new Button(buttonImage, buttonFont, "Редактор", buttonPosition);
            buttonEditor.Click += ButtonEditor_OnClick;
            buttonPosition.Y += buttonPositionOffsetY;

            var buttonSkins = new Button(buttonImage, buttonFont, "Скины", buttonPosition);
            buttonSkins.Click += ButtonSkins_OnClick;
            buttonPosition.Y += buttonPositionOffsetY;

            var buttonQuit = new Button(buttonImage, buttonFont, "Выход", buttonPosition);
            buttonQuit.Click += ButtonQuit_OnClick;
            buttonPosition.Y += buttonPositionOffsetY;

            stateComponents[GameState.MAIN_MENU] = new List<IComponent>()
            {
                background,
                header,
                buttonCampaign,
                buttonEditor,
                buttonSkins,
                buttonQuit
            };
        }

        private void ButtonCampaign_OnClick(object sender, EventArgs e)
        {
            state = GameState.PLAY_MENU;
        }

        private void ButtonEditor_OnClick(object sender, EventArgs e)
        {
            state = GameState.EDITOR_MENU;
        }

        private void ButtonSkins_OnClick(object sender, EventArgs e)
        {
            state = GameState.SKINS_MENU;
        }

        private void ButtonQuit_OnClick(object sender, EventArgs e)
        {
            Exit();
        }

        private void InitPlayMenu(SpriteFont headerFont, SpriteFont buttonFont, Texture2D buttonImage, Texture2D backgroundTexture)
        {
            var screenBorderMargin = 40;
            var menuTopMargin = 300;
            var buttonMargin = 10;

            var header = new Label(headerFont, "Campaign", new Vector2(screenBorderMargin, screenBorderMargin));
            var background = new Background(backgroundTexture,
                new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            var buttonPosition = new Vector2(graphics.PreferredBackBufferWidth / 2, menuTopMargin);
            var buttonPositionOffsetY = buttonImage.Height + buttonMargin;

            var buttonPlay = new Button(buttonImage, buttonFont, "Играть", buttonPosition);
            buttonPlay.Click += ButtonPlay_OnClick;
            buttonPosition.Y += buttonPositionOffsetY;

            var buttonRating = new Button(buttonImage, buttonFont, "Рейтинг", buttonPosition);
            buttonRating.Enabled = false;
            buttonPosition.Y += buttonPositionOffsetY;

            var buttonBack = new Button(buttonImage, buttonFont, "Назад", buttonPosition);
            buttonBack.Click += ButtonBack_OnClick;
            buttonPosition.Y += buttonPositionOffsetY;

            stateComponents[GameState.PLAY_MENU] = new List<IComponent>()
            {
                background,
                header,
                buttonPlay,
                buttonRating,
                buttonBack
            };
        }

        private void ButtonPlay_OnClick(object sender, EventArgs e)
        {
            state = GameState.SELECT_LEVEL_PLAY_MENU;
        }

        private void ButtonBack_OnClick(object sender, EventArgs e)
        {
            if (state == GameState.SELECT_LEVEL_PLAY_MENU)
            {
                state = GameState.PLAY_MENU;
            }
            if (state == GameState.SELECT_LEVEL_EDITOR_MENU)
            {
                state = GameState.EDITOR_MENU;
            }
            else
            {
                state = GameState.MAIN_MENU;
            }
        }

        private void InitSelectLevelPlayMenu(SpriteFont headerFont, SpriteFont buttonFont, Texture2D buttonImage, Texture2D backgroundTexture)
        {
            var screenBorderMargin = 40;

            var header = new Label(headerFont, "Select level", new Vector2(screenBorderMargin, screenBorderMargin));
            var background = new Background(backgroundTexture,
                new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            var buttonBackPosition = new Vector2(screenBorderMargin + buttonImage.Width / 2, graphics.PreferredBackBufferHeight - buttonImage.Height - screenBorderMargin);
            var buttonBack = new Button(buttonImage, buttonFont, "Назад", buttonBackPosition);
            buttonBack.Click += ButtonBack_OnClick;

            var components = new List<IComponent>()
            {
                background,
                header,
                buttonBack
            };

            var buttonLeftMargin = 60 + buttonImage.Width / 2;
            var buttonTopMargin = 160;
            var buttonMarginX = 32;
            var buttonMarginY = 10;
            var buttonPositionOffsetX = buttonImage.Width + buttonMarginX;
            var buttonPositionOffsetY = buttonImage.Height + buttonMarginY;

            var buttonPosition = new Vector2(buttonLeftMargin, buttonTopMargin);

            foreach (var map in gameMaps.Maps)
            {
                var button = new Button(buttonImage, buttonFont, map.Name, buttonPosition);
                button.Click += ButtonPlayLevel_OnClick;
                //todo disable and enable buttons for levels
                components.Add(button);
                buttonPosition.X += buttonPositionOffsetX;
                
                if (buttonPosition.X > graphics.PreferredBackBufferWidth)
                {
                    buttonPosition.X = buttonLeftMargin;
                    buttonPosition.Y += buttonPositionOffsetY;
                }
            }

            stateComponents[GameState.SELECT_LEVEL_PLAY_MENU] = components;
        }

        private void ButtonPlayLevel_OnClick(object sender, EventArgs e)
        {
            var mapName = ((Button)sender).Text;
            var map = gameMaps.GetMapByName(mapName);

            gameWindow.LoadUserSkins(playerSkins[user.CurrentPlayerSkinId], boxSkins[user.CurrentBoxSkinId], wallSkins[user.CurrentWallSkinId]);
            gameWindow.SetupMap(map);
            state = GameState.GAME;
        }

        private void InitGameWindow(Texture2D buttonTexture, SpriteFont font, Texture2D checkMark, Texture2D crossMark)
        {
            var buttonMarginLeft = 40 + buttonTexture.Width / 2;
            var buttonMarginTop = 20 + buttonTexture.Height / 2;
            var offsetX = 10 + buttonTexture.Width;
            var componentPosition = new Vector2(buttonMarginLeft, buttonMarginTop);

            var buttonQuit = new Button(buttonTexture, font, "Выход", componentPosition);
            buttonQuit.Click += ButtonPlay_OnClick;
            componentPosition.X += offsetX;

            var buttonContinue = new Button(buttonTexture, font, "Продолжить", componentPosition);
            buttonContinue.Click += ButtonContinue_OnClick;
            buttonContinue.Enabled = false;
            componentPosition.X += offsetX;

            var labelTime = new Label(font, "Время: 0", componentPosition, true);
            componentPosition.X += offsetX;
            var labelSteps = new Label(font, "Шаги: 0", componentPosition, true);

            var gameFieldOffsetY = 100;
            var gameField = new GameField(new Rectangle(0, gameFieldOffsetY, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight - gameFieldOffsetY), 
                checkMark, crossMark);

            gameWindow = new Components.GameWindow(buttonQuit, buttonContinue, labelTime, labelSteps, gameField);

            stateComponents[GameState.GAME] = new List<IComponent>()
            {
                gameWindow
            };
        }

        private void ButtonContinue_OnClick(object sender, EventArgs e)
        {
            gameWindow.SetupMap(gameMaps.GetNextMap());
            state = GameState.GAME;
        }

        private void InitEditorMenu(SpriteFont headerFont, SpriteFont buttonFont, Texture2D buttonImage, Texture2D backgroundTexture)
        {
            var screenBorderMargin = 40;
            var menuTopMargin = 300;
            var buttonMargin = 10;

            var header = new Label(headerFont, "Map editor", new Vector2(screenBorderMargin, screenBorderMargin));
            var background = new Background(backgroundTexture,
                new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            var buttonPosition = new Vector2(graphics.PreferredBackBufferWidth / 2, menuTopMargin);
            var buttonPositionOffsetY = buttonImage.Height + buttonMargin;

            var buttonCreate = new Button(buttonImage, buttonFont, "Создать", buttonPosition);
            buttonCreate.Enabled = false;   //todo creating maps for editor
            buttonPosition.Y += buttonPositionOffsetY;

            var buttonChange = new Button(buttonImage, buttonFont, "Изменить", buttonPosition);
            buttonChange.Click += ButtonChange_OnClick;
            buttonPosition.Y += buttonPositionOffsetY;

            var buttonBack = new Button(buttonImage, buttonFont, "Назад", buttonPosition);
            buttonBack.Click += ButtonBack_OnClick;
            buttonPosition.Y += buttonPositionOffsetY;

            stateComponents[GameState.EDITOR_MENU] = new List<IComponent>()
            {
                background,
                header,
                buttonCreate,
                buttonChange,
                buttonBack
            };
        }

        private void ButtonChange_OnClick(object sender, EventArgs e)
        {
            state = GameState.SELECT_LEVEL_EDITOR_MENU;
        }

        private void InitSelectLevelEditorMenu(SpriteFont headerFont, SpriteFont buttonFont, Texture2D buttonImage, Texture2D backgroundTexture)
        {
            var screenBorderMargin = 40;

            var header = new Label(headerFont, "Select level", new Vector2(screenBorderMargin, screenBorderMargin));
            var background = new Background(backgroundTexture,
                new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            var buttonBackPosition = new Vector2(screenBorderMargin + buttonImage.Width / 2, graphics.PreferredBackBufferHeight - buttonImage.Height - screenBorderMargin);
            var buttonBack = new Button(buttonImage, buttonFont, "Назад", buttonBackPosition);
            buttonBack.Click += ButtonBack_OnClick;

            var components = new List<IComponent>()
            {
                background,
                header,
                buttonBack
            };

            var buttonLeftMargin = 60 + buttonImage.Width / 2;
            var buttonTopMargin = 160;
            var buttonMarginX = 32;
            var buttonMarginY = 10;
            var buttonPositionOffsetX = buttonImage.Width + buttonMarginX;
            var buttonPositionOffsetY = buttonImage.Height + buttonMarginY;

            var buttonPosition = new Vector2(buttonLeftMargin, buttonTopMargin);

            foreach (var map in gameMaps.Maps)
            {
                var button = new Button(buttonImage, buttonFont, map.Name, buttonPosition);
                button.Click += ButtonEditLevel_OnClick;
                components.Add(button);
                buttonPosition.X += buttonPositionOffsetX;

                if (buttonPosition.X > graphics.PreferredBackBufferWidth)
                {
                    buttonPosition.X = buttonLeftMargin;
                    buttonPosition.Y += buttonPositionOffsetY;
                }
            }

            stateComponents[GameState.SELECT_LEVEL_EDITOR_MENU] = components;
        }

        private void ButtonEditLevel_OnClick(object sender, EventArgs e)
        {
            var mapName = ((Button)sender).Text;
            var map = gameMaps.GetMapByName(mapName);

            editorWindow.LoadUserSkins(playerSkins[user.CurrentPlayerSkinId], boxSkins[user.CurrentBoxSkinId], wallSkins[user.CurrentWallSkinId]);
            editorWindow.SetupMap(map);
            state = GameState.EDITOR;
        }

        private void InitEditorWindow(Texture2D buttonTexture, SpriteFont font, Texture2D btnPlayerTexture, Texture2D btnBoxTexture, 
            Texture2D btnCheckMarkTexture, Texture2D btnWallTexture, Texture2D btnCrossMarkTexture, Texture2D btnTrashMarkTexture)
        {
            var buttonMarginLeft = 40 + buttonTexture.Width / 2;
            var buttonMarginTop = 20 + buttonTexture.Height / 2;
            var offsetX = 10 + buttonTexture.Width;
            var componentPosition = new Vector2(buttonMarginLeft, buttonMarginTop);

            var buttonQuit = new Button(buttonTexture, font, "Выход", componentPosition);
            buttonQuit.Click += ButtonChange_OnClick;
            componentPosition.X += offsetX;

            var buttonSave = new Button(buttonTexture, font, "Сохранить", componentPosition);
            buttonSave.Click += ButtonSave_OnClick;
            componentPosition.X += offsetX;
            var buttonIconSize = 64;
            offsetX = 10 + buttonIconSize;

            var buttonPlayer = new Button(btnPlayerTexture, componentPosition, buttonIconSize, buttonIconSize);
            componentPosition.X += offsetX;
            var buttonBox = new Button(btnBoxTexture, componentPosition, buttonIconSize, buttonIconSize);
            componentPosition.X += offsetX;
            var buttonBoxOnGoal = new Button(btnCheckMarkTexture, componentPosition, buttonIconSize, buttonIconSize);
            componentPosition.X += offsetX;
            var buttonWall = new Button(btnWallTexture, componentPosition, buttonIconSize, buttonIconSize);
            componentPosition.X += offsetX;
            var buttonGoal = new Button(btnCrossMarkTexture, componentPosition, buttonIconSize, buttonIconSize);
            componentPosition.X += offsetX;
            var buttonBlank = new Button(btnTrashMarkTexture, componentPosition, buttonIconSize, buttonIconSize);

            var editorFieldOffsetY = 100;
            var editorField = new EditorField(new Rectangle(0, editorFieldOffsetY, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight - editorFieldOffsetY),
                btnCheckMarkTexture, btnCrossMarkTexture);

            editorWindow = new EditorWindow(buttonQuit, buttonSave, buttonPlayer, buttonBox, buttonBoxOnGoal, buttonWall, buttonGoal, buttonBlank, editorField);

            stateComponents[GameState.EDITOR] = new List<IComponent>()
            {
                editorWindow
            };
        }

        private void ButtonSave_OnClick(object sender, EventArgs e)
        {
            var newMap = editorWindow.NewMap;
            gameMaps.UpdateMapWithSameName(newMap);
            var path = $"{Content.RootDirectory}/level/{newMap.Name}.txt";
            newMap.SaveToFile(path);

            state = GameState.SELECT_LEVEL_EDITOR_MENU;
        }

        private void InitSkinsMenu(SpriteFont headerFont, SpriteFont buttonFont, Texture2D buttonImage)
        {
            var screenBorderMargin = 40;

            var header = new Label(headerFont, "Skins", new Vector2(screenBorderMargin, screenBorderMargin));
            var background = new Background(Content.Load<Texture2D>("background/bg-skins"),
                new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            var labelLeftMargin = 60;
            var labelTopMargin = 120;
            var labelOffsetY = 60;
            var labelPosition = new Vector2(labelLeftMargin, labelTopMargin);

            var labelPlayer = new Label(buttonFont, "Игрок:", labelPosition);
            labelPosition.Y += labelOffsetY;
            var labelBox = new Label(buttonFont, "Коробка:", labelPosition);
            labelPosition.Y += labelOffsetY;
            var labelWall = new Label(buttonFont, "Стена:", labelPosition);
            labelPosition.Y += labelOffsetY;
            var labelChest = new Label(buttonFont, "Лут боксы:", labelPosition);

            var buttonPosition = new Vector2(screenBorderMargin + buttonImage.Width/2, graphics.PreferredBackBufferHeight - buttonImage.Height - screenBorderMargin);
            var buttonBack = new Button(buttonImage, buttonFont, "Назад", buttonPosition);
            buttonBack.Click += ButtonBack_OnClick;

            stateComponents[GameState.SKINS_MENU] = new List<IComponent>()
            {
                background,
                header,
                labelPlayer,
                labelBox,
                labelWall,
                labelChest,
                buttonBack
            };
        }

        private void UpdateSkinsMenu()
        {
            var skinLeftMargin = 240;
            var skinTopMargin = 140;
            var skinSize = 50;
            var skinOffsetX = 10 + skinSize;
            var skinOffsetY = 10 + skinSize;
            var skinPosition = new Vector2(skinLeftMargin, skinTopMargin);
            var componentList = stateComponents[GameState.SKINS_MENU];

            foreach (var skinId in user.PlayerSkinIds)
            {
                componentList.Add(new Button(playerSkins[skinId], skinPosition, skinSize, skinSize));
                skinPosition.X += skinOffsetX;
            }

            skinPosition.X = skinLeftMargin;
            skinPosition.Y += skinOffsetY;

            foreach (var skinId in user.BoxSkinIds)
            {
                componentList.Add(new Button(boxSkins[skinId], skinPosition, skinSize, skinSize));
                skinPosition.X += skinOffsetX;
            }

            skinPosition.X = skinLeftMargin;
            skinPosition.Y += skinOffsetY;

            foreach (var skinId in user.WallSkinIds)
            {
                componentList.Add(new Button(wallSkins[skinId], skinPosition, skinSize, skinSize));
                skinPosition.X += skinOffsetX;
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            var components = stateComponents[state];

            foreach (var component in components)
            {
                component.Update(gameTime);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            var components = stateComponents[state];
            spriteBatch.Begin();

            foreach (var component in components)
            {
                component.Draw(gameTime, spriteBatch);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
