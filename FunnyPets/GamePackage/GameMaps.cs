﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunnyPets.GamePackage
{
    class GameMaps
    {
        public List<Map> Maps { get; }
        private readonly string directoryPath;
        private int currentMapIndex;

        public GameMaps(string directoryPath)
        {
            this.directoryPath = directoryPath ?? throw new ArgumentNullException(nameof(directoryPath));
            Maps = new List<Map>(9);
            var mapPaths = GetMapPaths();

            foreach (var path in mapPaths)
            {
                Maps.Add(Map.LoadFromFile(path));
            }
        }

        private List<string> GetMapPaths()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);
            FileInfo[] files = dirInfo.GetFiles("*.txt");
            var paths = new List<string>(files.Length);

            foreach (FileInfo file in files)
            {
                paths.Add(file.FullName);
            }

            return paths;
        }

        public Map GetMapByName(string name)
        {
            foreach (var map in Maps)
            {
                if (map.Name == name)
                {
                    currentMapIndex = Maps.IndexOf(map);
                    return map;
                }
            }

            currentMapIndex = 0;
            return null;
        }

        public void UpdateMapWithSameName(Map newMap)
        {
            for (int i = 0; i < Maps.Count; i++)
            {
                if (Maps[i].Name == newMap.Name)
                {
                    Maps[i] = newMap;
                }
            }
        }

        public Map GetNextMap()
        {
            if (++currentMapIndex == Maps.Count)
            {
                currentMapIndex = 0;
            }

            return Maps[currentMapIndex];
        }
    }
}
