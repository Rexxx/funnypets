﻿namespace FunnyPets.GamePackage
{
    enum GameState
    {
        NAME_INPUT,
        MAIN_MENU,
        PLAY_MENU,
        SELECT_LEVEL_PLAY_MENU,
        GAME,
        EDITOR_MENU,
        SELECT_LEVEL_EDITOR_MENU,
        EDITOR,
        SKINS_MENU
    }
}
