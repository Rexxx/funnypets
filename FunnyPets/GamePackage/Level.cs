﻿using System;
using Microsoft.Xna.Framework;

namespace FunnyPets.GamePackage
{
    class Level
    {
        public Entity[,] CurrentMap { get; }
        public int StepsCount { get; private set; }
        public double TimeInSec { get; set; }

        public Level(Map map)
        {
            if (map == null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            CurrentMap = Map.CopyEntities(map.Entities);

            StepsCount = 0;
            TimeInSec = 0;
        }

        public void TryToMovePlayer(Direction direction)
        {
            var playerPosition = GetPlayerPosition();
            Point forwardPosition = GetForwardPosition(playerPosition, direction);

            if (IsOutOfMap(forwardPosition))
            {
                return;
            }

            Entity forwardEntity = CurrentMap[forwardPosition.X, forwardPosition.Y];

            if (forwardEntity == Entity.Wall)
            {
                return;
            }

            var isPossibleToMove = true;

            if (IsBox(forwardEntity))
            {
                isPossibleToMove = TryToMoveBox(forwardPosition, direction);
            }

            if (isPossibleToMove)
            {
                ClearPlayerPosition(playerPosition);
                SetPlayerOnNewPosition(forwardPosition);

                StepsCount++;
            }
        }

        private bool IsOutOfMap(Point position)
        {
            return position.X < 0 || position.X >= CurrentMap.GetLength(0) || position.Y < 0 || position.Y >= CurrentMap.GetLength(1);
        }

        private Point GetForwardPosition(Point position, Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return new Point(position.X - 1, position.Y);
                case Direction.Down:
                    return new Point(position.X + 1, position.Y);
                case Direction.Left:
                    return new Point(position.X, position.Y - 1);
                case Direction.Right:
                    return new Point(position.X, position.Y + 1);
                default:
                    throw new Exception("Incorrect direction");
            }
        }

        private void ClearPlayerPosition(Point playerPosition)
        {
            if (CurrentMap[playerPosition.X, playerPosition.Y] == Entity.Player)
            {
                CurrentMap[playerPosition.X, playerPosition.Y] = Entity.Blank;
            }
            else if (CurrentMap[playerPosition.X, playerPosition.Y] == Entity.PlayerOnGoal)
            {
                CurrentMap[playerPosition.X, playerPosition.Y] = Entity.Goal;
            }
        }

        private void SetPlayerOnNewPosition(Point forwardPosition)
        {
            if (CurrentMap[forwardPosition.X, forwardPosition.Y] == Entity.Blank)
            {
                CurrentMap[forwardPosition.X, forwardPosition.Y] = Entity.Player;
            }
            else if (CurrentMap[forwardPosition.X, forwardPosition.Y] == Entity.Goal)
            {
                CurrentMap[forwardPosition.X, forwardPosition.Y] = Entity.PlayerOnGoal;
            }
        }

        private bool IsBox(Entity entity)
        {
            return entity == Entity.Box || entity == Entity.BoxOnGoal;
        }

        private bool TryToMoveBox(Point position, Direction direction)
        {
            Point forwardPosition = GetForwardPosition(position, direction);
            Entity forwardEntity = CurrentMap[forwardPosition.X, forwardPosition.Y];

            if (forwardEntity == Entity.Blank || forwardEntity == Entity.Goal)
            {
                ClearBoxPosition(position);
                SetBoxOnNewPosition(forwardPosition);
                return true;
            }

            return false;
        }

        private void ClearBoxPosition(Point position)
        {
            if (CurrentMap[position.X, position.Y] == Entity.Box)
            {
                CurrentMap[position.X, position.Y] = Entity.Blank;
            }
            else if (CurrentMap[position.X, position.Y] == Entity.BoxOnGoal)
            {
                CurrentMap[position.X, position.Y] = Entity.Goal;
            }
        }

        private void SetBoxOnNewPosition(Point forwardPosition)
        {
            if (CurrentMap[forwardPosition.X, forwardPosition.Y] == Entity.Blank)
            {
                CurrentMap[forwardPosition.X, forwardPosition.Y] = Entity.Box;
            }
            else if (CurrentMap[forwardPosition.X, forwardPosition.Y] == Entity.Goal)
            {
                CurrentMap[forwardPosition.X, forwardPosition.Y] = Entity.BoxOnGoal;
            }
        }

        private Point GetPlayerPosition()
        {
            for (int i = 0; i < CurrentMap.GetLength(0); i++)
            {
                for (int j = 0; j < CurrentMap.GetLength(1); j++)
                {
                    var entity = CurrentMap[i, j];

                    if (entity == Entity.Player || entity == Entity.PlayerOnGoal)
                    {
                        return new Point(i, j);
                    }
                }
            }

            throw new Exception("There is no Player on the map ._.");
        }

        public bool IsLevelCompleted()
        {
            foreach (var entity in CurrentMap)
            {
                if (entity == Entity.Box)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
