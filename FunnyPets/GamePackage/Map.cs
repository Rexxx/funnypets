﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FunnyPets.GamePackage
{
    class Map
    {
        public Entity[,] Entities { get; }
        public string Name { get; private set; }

        public Map(Entity[,] entities, string name)
        {
            if (entities == null)
            {
                throw new ArgumentNullException(nameof(entities));
            }

            if (name == string.Empty)
            {
                throw new ArgumentException(nameof(name), "Name can't be empty.");
            }

            if (!IsCorrectMap(entities))
            {
                throw new ArgumentException(nameof(entities), "Map is not correct!");
            }

            Entities = entities;
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public static Entity[,] CopyEntities(Entity[,] entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException(nameof(entities));
            }

            var rowsCount = entities.GetLength(0);
            var colsCount = entities.GetLength(1);
            var copy = new Entity[rowsCount, colsCount];

            for (var i = 0; i < rowsCount; i++)
            {
                for (var j = 0; j < colsCount; j++)
                {
                    copy[i, j] = entities[i, j];
                }
            }

            return copy;
        }

        public static Map LoadFromFile(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            var lines = new List<string>();
            var maxWidth = 0;
            
            using (StreamReader file = new StreamReader(path))
            {
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    lines.Add(line);

                    if (line.Length > maxWidth)
                    {
                        maxWidth = line.Length;
                    }
                }
            }

            return new Map(Create(lines, maxWidth), GetName(path));
        }

        public void SaveToFile(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            using (StreamWriter file = new StreamWriter(path))
            {
                var height = Entities.GetLength(0);
                var width = Entities.GetLength(1);

                for (var i = 0; i < height; i++)
                {
                    for (var j = 0; j < width; j++)
                    {
                        file.Write(GetSymbolByEntity(Entities[i, j]));
                    }

                    file.Write('\n');
                }
            }
        }

        public static string GetName(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            var names = path.Split('\\');

            return names[names.Length - 1].Replace(".txt", "");
        }

        private static Entity[,] Create(List<string> lines, int mapWidth)
        {
            if (lines.Count == 0)
            {
                throw new ArgumentException(nameof(lines), "Lines can't be empty.");
            }

            var map = new Entity[lines.Count, mapWidth];

            for (var i = 0; i < lines.Count; i++)
            {
                for (var j = 0; j < lines[i].Length; j++)
                {
                    map[i, j] = GetEntityBySmbol(lines[i][j]);
                }

                for (var k = lines[i].Length; k < mapWidth; k++)
                {
                    map[i, k] = Entity.Blank;
                }
            }

            return map;
        }

        private static Entity GetEntityBySmbol(char symbol)
        {
            switch (symbol)
            {
                case ' ':
                    return Entity.Blank;
                case '#':
                    return Entity.Wall;
                case '$':
                    return Entity.Box;
                case '.':
                    return Entity.Goal;
                case '@':
                    return Entity.Player;
                case '*':
                    return Entity.BoxOnGoal;
                default:
                    throw new Exception($"Incorrect symbol '{symbol}' for creating Entity.");
            }
        }

        private static char GetSymbolByEntity(Entity entity)
        {
            switch (entity)
            {
                case Entity.Blank:
                    return ' ';
                case Entity.Wall:
                    return '#';
                case Entity.Box:
                    return '$';
                case Entity.Goal:
                    return '.';
                case Entity.Player:
                    return '@';
                case Entity.BoxOnGoal:
                    return '*';
                default:
                    throw new Exception($"Incorrect entity for getting symbol.");
            }
        }

        public static bool IsCorrectMap(Entity[,] entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException(nameof(entities));
            }

            var playersCount = 0;
            var boxesCount = 0;
            var goalsCount = 0;

            foreach (var element in entities)
            {
                if (element == Entity.Player)
                {
                    playersCount++;
                }
                else if (element == Entity.Box)
                {
                    boxesCount++;
                }
                else if (element == Entity.Goal)
                {
                    goalsCount++;
                }
            }

            return playersCount == 1 && boxesCount == goalsCount;
        }

        public void EditEntity(int row, int col, Entity entity)
        {
            if (row < 0 || row >= Entities.GetLength(0))
            {
                throw new ArgumentOutOfRangeException(nameof(row), "Index should be in range of 0 and Entities.GetLength(0).");
            }

            if (col < 0 || col >= Entities.GetLength(1))
            {
                throw new ArgumentOutOfRangeException(nameof(col), "Index should be in range of 0 and Entities.GetLength(1).");
            }

            Entities[row, col] = entity;
        }
    }
}
