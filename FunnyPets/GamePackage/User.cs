﻿using System.Collections.Generic;

namespace FunnyPets.GamePackage
{
    class User
    {
        public string Name { get; private set; }
        public int Level { get; set; }
        public int TotalWalkthrougSeconds { get; set; }
        public int TotalStepsCount { get; set; }
        public List<int> PlayerSkinIds { get; }
        public List<int> BoxSkinIds { get; }
        public List<int> WallSkinIds { get; }
        public int CurrentPlayerSkinId { get; set; }
        public int CurrentBoxSkinId { get; set; }
        public int CurrentWallSkinId { get; set; }
        public int PlayerLootBoxCount { get; set; }
        public int BoxLootBoxCount { get; set; }
        public int WallLootBoxCount { get; set; }


        public User(string name)
        {
            Name = name;
            Level = 0;
            TotalWalkthrougSeconds = 0;
            PlayerSkinIds = new List<int> { 0 };
            BoxSkinIds = new List<int> { 0 };
            WallSkinIds = new List<int> { 0 };
            CurrentPlayerSkinId = 0;
            CurrentBoxSkinId = 0;
            CurrentWallSkinId = 0;
            PlayerLootBoxCount = 3;
            BoxLootBoxCount = 2;
            WallLootBoxCount = 0;
        }
    }
}
